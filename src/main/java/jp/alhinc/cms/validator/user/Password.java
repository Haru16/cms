package jp.alhinc.cms.validator.user;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


//付与する場所を指定(フィールド・メソッド...)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {PasswordValidator.class})
//アノテーションクラス
public @interface Password {

	//messageFieldにはエラーメッセージが入る
	String message() default "Invalid Password";

	//引数に使う型を宣言
	Class<?>[] groups() default {};

	//Payload荷重
	Class<? extends Payload>[] payload() default {};
}
