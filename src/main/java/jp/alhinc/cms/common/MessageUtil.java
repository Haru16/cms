package jp.alhinc.cms.common;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component//注入するクラスに付けるDIコンテナが管理してくれる
public final class MessageUtil {

	static MessageSource instance;

	@Autowired//注入されるフィールドに付ける
	private MessageSource source;

	@PostConstruct//WASが上がるときに実行される,beanが何回も初期化されることを防ぐ
	private void init() {
		MessageUtil.instance = source;
	}

	/**
	 * メッセージIDに該当するメッセージを返却します.
	 * @param id メッセージID
	 * @return メッセージ
	 */
	public static String getMessage(String id) {
		return instance.getMessage(removeBrace(id), new Object[]{}, Locale.getDefault());
	}

	/**
	 * メッセージIDに該当するメッセージを取得し、パラメータの文字列配列の値を埋め込んだメッセージを返却します.
	 * @param id メッセージID
	 * @param embededStrings 埋め込み文字列配列
	 * @return メッセージ
	 */
	public static String getMessage(String id, String... embededStrings) {
		return instance.getMessage(removeBrace(id), embededStrings, Locale.getDefault());
	}

	private static String removeBrace(String source) {
		return source.replace("{", "").replace("}", "");
	}
}
