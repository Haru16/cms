package jp.alhinc.cms.common;

/**
 * メッセージIDを管理します.
 */
public interface MessageId {
	/** 成功 */
	public static final String SUCCESS = "{SUCCESS}";

	/** ログインID または パスワード が間違っています。 */
	public static final String INF_001 = "{INF_001}";
	/** 必須項目が未入力です。 */
	public static final String INF_002 = "{INF_002}";
	/** 現在このユーザでシステムにログインすることは出来ません。システム管理者に確認して下さい。 */
	public static final String INF_003 = "{INF_003}";
	/** 現在このユーザでシステムにログインすることは出来ません。しばらくしてから再ログインをお願いします。 */
	public static final String INF_004 = "{INF_004}";
	/** 使用できない文字が入力されています。 */
	public static final String INF_005 = "{INF_005}";
	/** ログアウトしてもよろしいですか？ */
	public static final String INF_006 = "{INF_006}";
	/** パスワードには半角英大文字、半角英小文字、半角数字または記号を含んで下さい。 */
	public static final String INF_007 = "{INF_007}";
	/** 前回と同じパスワードは設定できません。 */
	public static final String INF_008 = "{INF_008}";
	/** セッションタイムアウトが発生しました。再度、ログインして下さい。 */
	public static final String INF_009 = "{INF_009}";
	/** 現在のパスワードが間違っています。*/
	public static final String INF_010 = "{INF_010}";
	/**新しいパスワードと新しいパスワード（確認用）が異なっています。*/
	public static final String INF_011 = "{INF_011}";
	/** パスワードは8～20文字以内で設定して下さい。 */
	public static final String INF_012 = "{INF_012}";
	/** システムエラー。システム管理者に連絡して下さい。 */
	public static final String ERR_001 = "{ERR_001}";
}
