package jp.alhinc.cms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/customers")
public class CustomersController extends AuthenticatedController {

	@GetMapping
	public String index() {
		return "customers/index";
	}

	@GetMapping("/create")
	public String create() {
		return "customers/create";
	}

}
