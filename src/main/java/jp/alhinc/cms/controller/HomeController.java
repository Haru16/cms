package jp.alhinc.cms.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class HomeController extends AuthenticatedController {

	@GetMapping
	public String index() {
		return "index";
	}

	@GetMapping("/aaa")
	@ResponseBody
	@Secured({"hasAutority('CUSTOMER_SERVICE')"})
	public String aaa() {
		return "aaa";
	}

	@GetMapping("/bbb")
	@ResponseBody
	@Secured({"ROLE_BACK_OFFICE"})
	public String bbb() {
		return "bbb";
	}

}
