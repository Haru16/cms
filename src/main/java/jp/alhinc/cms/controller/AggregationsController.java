package jp.alhinc.cms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/aggregations")
public class AggregationsController extends AuthenticatedController {

	@GetMapping("/balance/bycustomer")
	public String balanceByCustomer() {
		return "aggregations/balancebycustomer";
	}

	@GetMapping("/balance/byaccount")
	public String balanceByAccount() {
		return "aggregations/balancebyaccount";
	}

	@GetMapping("/analysis/bystate")
	public String analysisByState() {
		return "aggregations/analysisbystate";
	}

	@GetMapping("/analysis/byaccount")
	public String analysisByAccount() {
		return "aggregations/analysisbyaccount";
	}

}
