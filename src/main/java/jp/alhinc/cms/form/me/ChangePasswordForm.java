package jp.alhinc.cms.form.me;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import jp.alhinc.cms.common.MessageId;
import jp.alhinc.cms.validator.user.Password;

public class ChangePasswordForm implements Serializable {

	//未入力
	@NotEmpty(message = MessageId.INF_002)
	private String currentPassword;

	//未入力
	@NotEmpty(message = MessageId.INF_002)
	//文字数
	@Size(min=8,max=20,message= MessageId.INF_012)
	//使用できない文字
	@Password(message= MessageId.INF_005)
	private String newPassword;

	//未入力
	@NotEmpty(message = MessageId.INF_002)
	private String passwordConfirmation;

	//新PWと確認用PWの一致確認
	@AssertTrue(message = MessageId.INF_011)
	public boolean isPasswordValid() {
		if(newPassword == null || newPassword.isEmpty() && passwordConfirmation == null || passwordConfirmation.isEmpty())
			return true;
		return newPassword.equals(passwordConfirmation);
	}

	//パスワードには半角英大文字、半角英小文字、半角数字または記号を含んで下さい。
	@AssertTrue(message = MessageId.INF_007)
	public boolean isSecondValid() {
		if(newPassword != null) {
			if(!(newPassword.matches(".*[a-z].*"))) {
				return false;
			} else if (!(newPassword.matches(".*[A-Z].*"))) {
				return false;
			} else if (!(newPassword.matches(".*[0-9!\\\"#$%&‘()*+,-./:;<=>?@[￥]^_`{|}~].*"))) {
				return false;
			} else {
				return true;
			}
		}
			return true;
	}

	//変更前PWとの相違
	@AssertTrue(message = MessageId.INF_008)
	public boolean isCurrentValid() {
		if(currentPassword == null || currentPassword.isEmpty() && newPassword == null || newPassword.isEmpty())
			return true;
		return !currentPassword.equals(newPassword);
	}

	/**
	 * @return currentPassword
	 */
	public String getCurrentPassword() {
		return currentPassword;
	}

	/**
	 * @param currentPassword セットする currentPassword
	 */
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	/**
	 * @return newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword セットする newPassword
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return passwordConfirmation
	 */
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	/**
	 * @param passwordConfirmation セットする passwordConfirmation
	 */
	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
}