package jp.alhinc.cms.service.me;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.cms.entity.User;
import jp.alhinc.cms.form.me.ChangePasswordForm;
import jp.alhinc.cms.mapper.UserMapper;

@Service
public class ChangePasswordService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public int updatePassword(Long id, ChangePasswordForm form) {
		//User情報
		User target = mapper.findById(id);
		System.out.println("fffffffff");
		BCryptPasswordEncoder bccryptPassword = new BCryptPasswordEncoder();
//		一致しているか比較
		if (!bccryptPassword.matches(form.getNewPassword(),target.getPassword())) {
			// TODO : エラーコード返す
			return 0;
		}
		//update
		return mapper.updatePassword(id, new BCryptPasswordEncoder().encode(form.getNewPassword()));
	}
}
